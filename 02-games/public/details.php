<?php

require_once(__DIR__.'/../lib/controller/IndexController.php');

$id = $_GET['g'];

$cnt = new IndexController();
$ins = $cnt->gameDetails($id);

?><html>
  <head>
    <title>Game details</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Game details</h1>
      <ul>
      	<li><?=$ins->getGid()?></li>
      	<li><?=$ins->getTitle()?></li>
        <li><?=$ins->getCategory()?></li>
        <li><?=$ins->getRelease()?></li>
      </ul>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>