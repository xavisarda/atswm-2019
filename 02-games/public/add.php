<html>
  <head>
    <title>Add game</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Game list</h1>
      <form action="/forms/add.php" method="post">
      	<dl>
      	 <dt><label for="gtitle">Title</label></dt>
      	 <dd><input type="text" id="gtitle" name="gtitle"/></dd>
      	 <dt><label for="gry">Release year</label></dt>
      	 <dd><input type="text" id="gry" name="gry"/></dd>
      	 <dt><label for="gcat">Category</label></dt>
      	 <dd>
      	 	<select id="gcat" name="gcat">
      	 		<option value="Sports">Sports</option>
      	 		<option value="Battle Royal">Battle Royal</option>
      	 		<option value="Platform">Platform</option>
      	 		<option value="Graphic Adventure">Graphic Adventure</option>
      	 	</select>
  	 	</dd>
  	 	<dd><input type="submit" name="gsub" value="Create"/></dd>
      	</dl>
      
      </form>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>