<?php 

require_once(__DIR__.'/../lib/controller/IndexController.php');

$id = $_GET['g'];

$cnt = new IndexController();
$ins = $cnt->gameDetails($id);

$cnt->removeGame($id);

?><html>
  <head>
    <title>Game removed</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Game removed</h1>
      <ul>
      	<li><?=$ins->getGid()?></li>
      	<li><?=$ins->getTitle()?></li>
        <li><?=$ins->getCategory()?></li>
        <li><?=$ins->getRelease()?></li>
      </ul>
      <a href="/">Go back to index</a>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>