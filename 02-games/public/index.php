<?php

require_once(__DIR__.'/../lib/controller/IndexController.php');
require_once(__DIR__.'/../lib/controller/UserController.php');

session_start();

$cnt = new IndexController();
$list = $cnt->listAction();

?><html>
  <head>
    <title>Game list</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../lib/inc/header.php'); ?>
      <h1>Game list</h1>
      <table>
        <tr>
          <th>Title</th>
          <th>Category</th>
          <th>Release year</th>
          <th>&nbsp;</th>
          <th>&nbsp;</th>
        </tr>
<?php foreach ($list as $g){ ?>
        <tr>
          <td>
          <a href="/details.php?g=<?=$g->getGid()?>"><?=$g->getTitle()?></a>
          </td>
          <td><?=$g->getCategory()?></td>
          <td><?=$g->getRelease()?></td>
          <td><a href="/update.php?g=<?=$g->getGid()?>">Modificar</a></td>
          <td><a href="/delete.php?g=<?=$g->getGid()?>">Eliminar</a></td>
        </tr>
<?php } ?>
      </table>
      <?php include(__DIR__.'/../lib/inc/footer.php'); ?>
    </div>
  </body>
</html>