<?php

require_once(__DIR__.'/../lib/controller/IndexController.php');

$id = $_GET['g'];

$cnt = new IndexController();
$ins = $cnt->gameDetails($id);

?><html>
  <head>
    <title>Update game</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Update game</h1>
      <form action="/forms/update.php" method="post">
      	<dl>
      	 <dt><label for="gtitle">Title</label></dt>
      	 <dd><input type="text" id="gtitle" name="gtitle" value="<?=$ins->getTitle()?>"/></dd>
      	 <dt><label for="gry">Release year</label></dt>
      	 <dd><input type="text" id="gry" name="gry" value="<?=$ins->getRelease()?>"/></dd>
      	 <dt><label for="gcat">Category</label></dt>
      	 <dd>
      	 	<select id="gcat" name="gcat">
      	 		<option value="Sports" <?php if($ins->getCategory() == "Sports"){ echo "selected";}?>>Sports</option>
      	 		<option value="Battle Royal" <?php if($ins->getCategory() == "Battle Royal"){ echo "selected";}?>>Battle Royal</option>
      	 		<option value="Platform" <?php if($ins->getCategory() == "Platform"){ echo "selected";}?>>Platform</option>
      	 		<option value="Graphic Adventure" <?php if($ins->getCategory() == "Graphic Adventure"){ echo "selected";}?>>Graphic Adventure</option>
      	 	</select>
  	 	</dd>
  	 	<dd>
  	 		<input type="hidden" name="gid" value="<?=$id?>"/>
  	 		<input type="submit" name="gsub" value="Update"/>
  	 	</dd>
      	</dl>
      
      </form>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>