<?php

require_once(__DIR__.'/../../lib/controller/IndexController.php');

$gt = $_POST['gtitle'];
$gr = $_POST['gry'];
$gc = $_POST['gcat'];
$gi = $_POST['gid'];

$cnt = new IndexController();
$ins = $cnt->updateGame($gt, $gr, $gc, $gi);

?><html>
  <head>
    <title>Game updated</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Game updated</h1>
      <ul>
      	<li><?=$ins->getTitle()?></li>
        <li><?=$ins->getCategory()?></li>
        <li><?=$ins->getRelease()?></li>
      </ul>
      <a href="/">Go back to index</a>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>