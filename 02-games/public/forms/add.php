<?php

require_once(__DIR__.'/../../lib/controller/IndexController.php');

$gt = $_POST['gtitle'];
$gr = $_POST['gry'];
$gc = $_POST['gcat'];

$cnt = new IndexController();
$ins = $cnt->createGame($gt, $gr, $gc);

?><html>
  <head>
    <title>Game added</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include(__DIR__.'/../inc/header.php'); ?>
      <h1>Game added</h1>
      <ul>
      	<li><?=$ins->getTitle()?></li>
        <li><?=$ins->getCategory()?></li>
        <li><?=$ins->getRelease()?></li>
      </ul>
      <a href="/">Go back to index</a>
      <?php include(__DIR__.'/../inc/footer.php'); ?>
    </div>
  </body>
</html>