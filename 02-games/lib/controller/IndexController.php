<?php

require_once(__DIR__.'/../model/db/GameDb.php');

class IndexController{
    
    public function listAction(){
        $dbadapter = new GameDb();
        $list = $dbadapter->listGames();
        
        return $list;
    }
    
    public function gameDetails($id){
        $dbadapter = new GameDb();
        $game = $dbadapter->getGameById($id);
        
        return $game;
    }
    
    public function createGame($title, $releasey, $category){
        $dbadapter = new GameDb();
        $game = $dbadapter->insertGame($title, $releasey, $category);
        
        return $game;
    }
    
    public function removeGame($id){
        $dbadapter = new GameDb();
        $game = $dbadapter->deleteGame($id);
        
        return;
    }
    
    public function updateGame($title, $releasey, $category, $id){
        $dbadapter = new GameDb();
        $game = $dbadapter->updateGame($title, $releasey, $category, $id);
        
        return $game;
    }
    
}