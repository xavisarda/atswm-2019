<?php 

require_once(__DIR__.'/../model/db/UserDb.php');

class UserController{
    
    public function login($u, $p){
        //Cerca a la DDBB
        $db = new UserDb();
        if($db->login($u, $p)){
            $_SESSION['user'] = $u;
            return true;
        }
        return false;
    }
    
    public function isUserLoggedIn(){
        if(isset($_SESSION['user']) 
            && $_SESSION['user']!= null 
            && $_SESSION['user'] != ""){
            return true;
        }
        return false;
    }
    
    public function userName(){
        return $_SESSION['user'];
    }
    
}