<?php 

require_once(__DIR__.'/../Game.php');

class GameDb{
    
    private $conn;
    
    public function listGames(){
        $this->openConnection();
        $sql = "SELECT * FROM game";
        $stm = $this->conn->prepare($sql);
        
        $stm->execute();
        $result = $stm->get_result();
        
        $ret = array();
        while($r = $result->fetch_assoc()){
            $curr = new Game($r['title'], $r['releasey'],
                $r['category'], $r['gid']);
            array_push($ret, $curr);
        }
        return $ret;
    }
    
    public function getGameById($id){
        $this->openConnection();
        $sql = "SELECT * FROM game WHERE gid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $gid);
        $gid = $id;
        
        $stm->execute();
        $result = $stm->get_result();
        
        $r = $result->fetch_assoc();
        $curr = new Game($r['title'], $r['releasey'],$r['category'], $r['gid']);
         
        return $curr;
    }
    
    public function insertGame($title, $ry, $cat){
        $this->openConnection();
        $sql = "INSERT INTO game (title, releasey, category) VALUES (?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sss", $gt, $gr, $gc);
        $gt = $title;
        $gr = $ry;
        $gc = $cat;
        
        $stm->execute();
        
        
        return new Game($title, $ry, $cat);
    }
    
    public function deleteGame($id){
        $this->openConnection();
        $sql = "DELETE FROM game WHERE gid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $gid);
        $gid = $id;
        
        $stm->execute();
        
        
        return;
    }
    
    public function updateGame($title, $ry, $cat, $id){
        $this->openConnection();
        $sql = "UPDATE game SET title = ?, releasey = ?, category = ? WHERE gid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sssi", $gt, $gr, $gc, $gid);
        $gt = $title;
        $gr = $ry;
        $gc = $cat;
        $gid = $id;
        
        $stm->execute();
        
        return $this->getGameById($id);
    }
    
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1",
                "gameuser",
                "G4m3#2019",
                "games");
        }
    }
    
}

/*
    
    array_push($ret, new Game('FIFA19','2018','Sports',1));
       array_push($ret, new Game('ProEvolutionSoccer 19','2018','Sports',2));
       array_push($ret, new Game('Futbol Manager 19','2018','Sports',3));
       array_push($ret, new Game('Fortnite','2017','Battle Royal',4));
       array_push($ret, new Game('Sonic','1991','Platform',5));
       array_push($ret, new Game('GTA V','2015','Graphic Adventure',6));
       
       */