<?php 

class Game{
    
    private $_gid;
    private $_title;
    private $_release;
    private $_category;
    
    public function __construct($t = null, $r = null, $c = null, $id = null){
        $this->setTitle($t);
        $this->setRelease($r);
        $this->setCategory($c);
        $this->setGid($id);
    }
    
    
    public function getGid()
    {
        return $this->_gid;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getRelease()
    {
        return $this->_release;
    }

    public function getCategory()
    {
        return $this->_category;
    }

    public function setGid($_gid)
    {
        $this->_gid = $_gid;
    }

    public function setTitle($_title)
    {
        $this->_title = $_title;
    }

    public function setRelease($_release)
    {
        $this->_release = $_release;
    }

    public function setCategory($_category)
    {
        $this->_category = $_category;
    }
    
}