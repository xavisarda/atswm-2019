create database games;

create user 'gameuser'@'%' identified by 'G4m3#2019';

grant all on games.* to gameuser;

use games;

create table game ( gid INTEGER AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), releasey VARCHAR(255), category VARCHAR(255) );

insert into game (title, releasey, category) values ('FIFA19','2018','Sports'),('ProEvolutionSoccer 19','2018','Sports'),('Futbol Manager 19','2018','Sports'),('Fortnite','2017','Battle Royal'),('Sonic','1991','Platform'),('GTA V','2015','Graphic Adventure');


