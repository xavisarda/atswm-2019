<?php

require_once(__DIR__.'/../model/Movie.php');

class MoviesController{

	public function indexAction(){
		$ret = array();
		$m1 = new Movie('Shutter Island','2005');
		$m2 = new Movie('Jurassic Park','1992');

		array_push($ret, $m1, $m2);
		return $ret;
		
	}

}


