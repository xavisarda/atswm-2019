<?php

class Movie{


	private $_title;
	private $_year;

	public function __construct($t = null, $y = null){
		$this->_title = $t;
		$this->_year = $y;
	}

	public function getTitle(){
		return $this->_title;
	}

	public function getYear(){
		return $this->_year;
	}


}

