<?php

require_once(__DIR__.'/../lib/controller/MoviesController.php');

$cnt = new MoviesController();
$list = $cnt->indexAction();

?><html>
  <head>
    <title>Projecte - Require Include</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include('header.php'); ?>
      <div id="content">
	<h1>Content</h1>
	<ul>
<?php foreach($list as $m){ ?>
          <li><?=$m->getTitle()?>-<?=$m->getYear()?></li>
<?php } ?>
        </ul>
      </div>
      <?php include('footer.php'); ?>
    </div>
  </body>
</html>
