<html>
  <head>
    <title>Projecte - Require Include</title>
  </head>
  <body>
    <div id="wrapper">
      <?php include('header.php'); ?>
      <div id="content">
	<h1>Content</h1>
	<p>Spicy jalapeno bacon ipsum dolor amet leberkas beef salami brisket. Porchetta alcatra flank picanha t-bone prosciutto jerky cupim landjaeger. Doner brisket t-bone, ham hock biltong bacon jowl cupim jerky capicola fatback. Pork loin ham hock bresaola, flank ground round pastrami kevin tail chuck beef ribs short loin.

Short loin tongue pork belly, sirloin cupim boudin kielbasa. Pork belly sausage pastrami meatball boudin corned beef. Shoulder pork loin short ribs, biltong meatball tail t-bone. Cupim alcatra short ribs salami, ham sausage strip steak capicola. Short loin ribeye t-bone, tenderloin biltong ham capicola. Turducken biltong porchetta kielbasa cupim meatloaf jowl ribeye shankle prosciutto. Pig tenderloin pork leberkas, pork chop alcatra shoulder spare ribs.

Pork doner ball tip, bresaola ground round jowl cow drumstick tail frankfurter filet mignon rump. Boudin jowl short ribs capicola shank, strip steak tri-tip drumstick picanha brisket turducken shankle salami beef. Pork swine cow turducken, ham pastrami flank hamburger ribeye beef ribs andouille pancetta sausage. Leberkas hamburger andouille, pig strip steak pastrami t-bone. Beef ribs t-bone bresaola shoulder chuck, ground round biltong.

Bresaola doner jerky kevin tri-tip. Pig spare ribs ball tip filet mignon. Hamburger turkey landjaeger kielbasa. Leberkas t-bone meatloaf chicken shoulder capicola ground round boudin porchetta swine. Bresaola frankfurter sirloin pork chop.

Drumstick ground round strip steak shankle short loin meatloaf. Salami filet mignon short loin cow, tongue venison strip steak ball tip sirloin pork belly pastrami t-bone kevin. Kielbasa fatback swine meatloaf tenderloin jerky shankle ground round tongue beef tri-tip ham hock sirloin jowl. Spare ribs filet mignon swine brisket pork chop rump short loin kevin pork belly prosciutto capicola jerky short ribs frankfurter. Corned beef biltong filet mignon pork chop capicola short ribs turkey boudin porchetta beef tri-tip pork belly kevin chuck. Ham hock capicola meatball picanha kevin cow biltong venison ham shankle leberkas pork boudin.</p>
      </div>
      <?php include('footer.php'); ?>
    </div>
  </body>
</html>
