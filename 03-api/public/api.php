<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require(__DIR__.'/../lib/ext/vendor/autoload.php');
require_once(__DIR__.'/../lib/controller/IndexController.php');

$app = new \Slim\App;

$app->get('/game', function (Request $request, Response $response, array $args) {
    $cnt = new IndexController();
    $games = $cnt->listAction();
    $retArray = [];
    foreach($games as $g){
        array_push($retArray, $g->toArray());
    }
    
    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retArray));

    return $newR;
});

$app->post('/game', function (Request $request, Response $response, array $args) {
    $cnt = new IndexController();
    
    $gamejson = $request->getParsedBody();
    $ret = $cnt->createGame($gamejson['title'], $gamejson['release'], $gamejson['category']);
    
    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($ret->toArray()));
    
    return $newR;
});

$app->get('/game/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    
    $cnt = new IndexController();
    $game = $cnt->gameDetails($id);
    $response->getBody()->write(json_encode($game->toArray()));
    
    return $response;
});


$app->run();

