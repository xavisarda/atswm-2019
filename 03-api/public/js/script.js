
function createGame(){
	var game = { "title" : $('#title').val(),
				"release" : $('#release').val(),
				"category" : $('#category').val()};
	$.ajax({
		"url":"/game",
		"method":"post",
		"data" : game,
		"dataType" : "application/json",
		"accept":"application/json"
	}).done(function(response){
		loadGamesHome();
	});
	
}

function loadGamesHome(){
	$.ajax({
		"url":"/game",
		"method":"get",
		"accept":"application/json"
	}).done(function(response){
		var htmlgame = '';
		$.each(response, function(index,val){
			htmlgame += '<article>';
			htmlgame += '<p>'+val.category+'</p>';
			htmlgame += '<h3>'+val.title+'</h3>';
			htmlgame += '<div>'+val.release+'</div>';
			htmlgame += '</article>';
		});
		$('#listgames').html(htmlgame);
	});
}
