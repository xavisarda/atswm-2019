<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require(__DIR__.'/../lib/ext/vendor/autoload.php');

$app = new \Slim\App;

$app->get('/acb/{name}', function (Request $request, Response $response, array $args) {
    
    $name = $args['name'];
    $response->getBody()->write("El ganador de la ACB es $name");
    
    return $response;
});
    
$app->run();
    